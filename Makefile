#							     /   |/ __  \ 42 Lyon School
#							    / /| |`' / /' / /_ _____  ___   rcepre
#							   / /_| |  / /  / / // / _ \/ _ \  Makefile
#							   \___  |./ /__/_/\_, /\___/_//_/  18/12/2020
#							       |_/\_____/  Makefile


#COMPILER
CXX =						clang++
CXXFLAGS =					-Wall -Wextra -Werror -std=c++17
DEBUG_FLAGS =				-g3 -Og
RELEASE_FLAGS =				-O3 -DNDEBUG
LDFLAGS =

ifeq ($(RELEASE), 1)
        CXXFLAGS +=			$(RELEASE_FLAGS)
else
        CXXFLAGS +=			$(DEBUG_FLAGS)
endif

#NAME
NAME =						abstract-vm

# SOURCES
SRCS =						$(addprefix $(SRCS_DIR), $(SRCS_FILES))
SRCS_DIR =					src/
SRCS_FILES =				main.cpp \
                            OperandFactory.cpp \
                            OperandsExceptions.cpp \
                            BaseException.cpp \
                            Machine.cpp \
                            Action.cpp \
                            Parser.cpp \
                            Syntax.cpp

# HEADERS
HEADERS =					-I src/

#OBJECTS
OBJS_DIR =					objects/
OBJS_NAME =					$(SRCS_FILES:.cpp=.o)
OBJS =						$(addprefix $(OBJS_DIR), $(OBJS_NAME))

#DEPENDENCIES
DEPS =						$(OBJS:%.o=%.d)

#RULES
all: $(NAME)

$(NAME): $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^


$(OBJS_DIR)%.o: $(SRCS_DIR)%.cpp
	@echo "Compiling $<"
	mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -MMD $(HEADERS) -o $@ -c $<

-include $(DEPS)

#CLEANING
clean:
	@echo "Clean .o .d"
	rm -rf $(OBJS_DIR)

fclean: clean
	@echo "Clean binaries"
	rm -rf $(NAME)

re: fclean all

.PHONY: clean fclean re