# abstract-vm

![purpose](https://img.shields.io/badge/purpose-education-green.svg)
![purpose](https://img.shields.io/badge/42-School-Blue.svg)

[42 school ] A simple virtual machine that can interpret programs written in a basic assembly language.

### Build

```shell
# Compile all
make
# Clean objects files
make clean
# Clean objestcs files and executable
make fclean
# Clean objects files and executable, then compile again
make re
# Use flags for Release (optimisation and bypass Assertions)
make re RELEASE=1
```

### Run

```shell
# Read from standart input
./abstract-vm
;; # <- Stops waiting for any input and execute.

#Read from file
./abstract-vm file.avm
```

### Usage

|  command  | parameter     | description                                                                                 |
| --------- | ------------- | ------------------------------------------------------------------------------------------- |
| exit      |               | Stop to execute command queue and exit.     |
| print     |               | Print the value at the top of the stack if is an 8-bit integer                          |
| assert    | type(value)   | Asserts that the value at the top of the stack is equal to the one passedas parameter for this instruction.                                                          |
| push      | type(value)   | Pushes the value v at the top of the stack. |
| pop       |               | Unstacks the value from the top of the stack                                                |
| add       |               | Unstacks the first two values on the stack, adds them, then stacks the result               |
| sub       |               | Unstacks the first two values on the stack, subtracts them, then stacks the result          |
| mul       |               | Unstacks the first two values on the stack, multiplies them, then stacks the result         |
| div       |               | Unstacks the first two values on the stack, divides them, then stacks the result            |
| mod       |               | Unstacks the first two values on the stack, calculates the modulus, then stacks the result  |
| Dump       |              | Print the content of the stack
| ;         |               | Comment                                                        |
| ;;        |               | Only for stdin mode: Stops waiting for any input and execute.                                                     |

##### Example

```shell
./abstract-vm
push int8(42)       ;push an integer on 8 bits with the value 42.
push double(42.0)   ;push a double precision float with the value 42.
add                 ;Unstack them, then stack result.
assert double(84.0)   ;Assert that the type is double and the value is 84.
dump                ;Dump the stack (should print 84)
exit                
;;
84
```
