#!/bin/sh

error=0

for file in $(find ./test_files/fails -type f -name "*.avm"); do
  echo "$file\c"
  ./abstract-vm $file | grep -q "Error"
  if [ $? -eq 0 ]; then
    echo " - fail OK"
  else
    echo " - /!\\ KO /!\\"
    error=1
  fi
done

for file in $(find ./test_files/succes -type f -name "*.avm"); do
  echo "$file\c"
  ./abstract-vm $file | grep -q "Error"
  if [ $? -eq 0 ]; then
    echo " - /!\\ KO /!\\"
    error=1
  else
    echo " - No error, OK"
  fi
done
if [ $error -ne 0 ]; then
  echo " -----> FAIL <-----"
  exit 1
else
  echo " -----> SUCCESS <-----"
fi
