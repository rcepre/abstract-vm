//     /   |/ __  \ 42 Lyon School                    
//    / /| |`' / /' / /_ _____  ___   rcepre      
//   / /_| |  / /  / / // / _ \/ _ \  abstract-vm
//   \___  |./ /__/_/\_, /\___/_//_/  11/12/2020
//       |_/\_____/  Parser.hpp

#pragma once

#include <iostream>
#include <sstream>
#include <vector>
#include "Action.hpp"
#include "OperandsExceptions.hpp"


class Parser {

private:
    inline static int _lineCount = 0;

    std::vector<Action> _actions;

    std::vector<BaseException> _errors;

public:
    Parser() = default;

    virtual ~Parser() = default;

    Parser &operator=(Parser const &rhs) = delete;

    Parser(Parser const &o) = delete;

    static Parser &get();

public:
    static const std::vector<Action> &getActions();

    static void readLine(std::string const &str);

    static void addError(BaseException const &e);

    static bool hasErrors();

    static std::vector<BaseException> getErrors();


private:

    void _addError(BaseException const &e);

    bool _hasErrors() const;

    std::vector<BaseException> _getErrors() const;

    const std::vector<Action> &_getActions() const;

    void _readLine(std::string const &str);

    static Action::eCommand _parseCommandKeyWord(std::string &str);

    static eOperandType _parseOpTypeKeyWord(std::string &s);

    static std::string _parseValue(std::string &s);

public:
//    EXCEPTIONS

    class InvalidCommandException : public BaseException {
    public:
        explicit InvalidCommandException(std::string const &s);;
    };

    class InvalidOpTypeException : public BaseException {
    public:
        explicit InvalidOpTypeException(std::string const &s);;
    };

    class InvalidValueException : public BaseException {
    public:
        explicit InvalidValueException(std::string const &s);;
    };

    class UnexpectedExpressionException : public BaseException {
    public:
        explicit UnexpectedExpressionException(std::string const &s);;
    };

};




