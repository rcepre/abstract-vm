//
// Created by rcepre on 24/11/2020.
//

#pragma once

#include <string>
#include <utility>
#include <unordered_map>

#define NB_OPT_TYPES 5

enum eOperandType {
    Int8, Int16, Int32, Float, Double
};

class IOperand {
public:

    virtual int getPrecision() const = 0;

    virtual eOperandType getType() const = 0;

    virtual IOperand const *operator+(IOperand const &rhs) const = 0;

    virtual IOperand const *operator-(IOperand const &rhs) const = 0;

    virtual IOperand const *operator*(IOperand const &rhs) const = 0;

    virtual IOperand const *operator/(IOperand const &rhs) const = 0;

    virtual IOperand const *operator%(IOperand const &rhs) const = 0;

    virtual std::string const &toString() const = 0;

    virtual ~IOperand() = default;

};
