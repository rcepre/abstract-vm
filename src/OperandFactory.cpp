//
// Created by rcepre on 27/11/2020.
//


#include "Operand.hpp"

OperandFactory::OperandFactory() {

    fPointers[Int8] = &OperandFactory::createInt8;
    fPointers[Int16] = &OperandFactory::createInt16;
    fPointers[Int32] = &OperandFactory::createInt32;
    fPointers[Float] = &OperandFactory::createFloat;
    fPointers[Double] = &OperandFactory::createDouble;
}


IOperand const *OperandFactory::createInt8(const std::string &value) const {
    auto tmp = std::stoi(value);
    Operand<int8_t>::checkLimits(tmp);
    return new Operand<int8_t>(static_cast<int8_t>(tmp));
}

IOperand const *OperandFactory::createInt16(const std::string &value) const {
    auto tmp = std::stoi(value);
    Operand<int16_t>::checkLimits(tmp);
    return new Operand<int16_t>(static_cast<int16_t>(tmp));
}

IOperand const *OperandFactory::createInt32(const std::string &value) const {
    auto tmp = std::stol(value);
    Operand<int32_t>::checkLimits(tmp);
    return new Operand<int32_t>(static_cast<int32_t>(tmp));
}

IOperand const *OperandFactory::createFloat(const std::string &value) const {
    auto tmp = std::stold(value);
    Operand<float>::checkLimits(tmp);
    return new Operand<float>(static_cast<float >(tmp));
}

IOperand const *OperandFactory::createDouble(const std::string &value) const {
    auto tmp = std::stold(value);
    Operand<double>::checkLimits(tmp);
    return new Operand<double>(static_cast<double>(tmp));
}


IOperand const *OperandFactory::createOperand(eOperandType type, const std::string &value) const {
    if (value.length() > 512) {
        throw TooLongStringException(value);
    }
    try {
        return (this->*fPointers[type])(value);
    } catch (std::invalid_argument &e) {
        throw BadStringFormatException(value);
    } catch (std::bad_alloc &e) {
        throw BadAllocException();
    } catch (std::out_of_range &e) {
        throw ValueOverflowException(value);
    }
}

OperandFactory const &OperandFactory::get() {
    static OperandFactory instance;
    return instance;
}

IOperand const *OperandFactory::create(eOperandType type, const std::string &value) {
    return get().createOperand(type, value);
}


