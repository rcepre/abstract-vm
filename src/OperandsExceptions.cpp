//
// Created by rcepre on 03/12/2020.
//

#include "OperandsExceptions.hpp"

ValueOverflowException::ValueOverflowException(long double value, eOperandType type) {
    _message = "Value overflow.";
    _type = "Runtime";
    if (type == Float || type == Double) {
        _source = std::to_string(value);
    } else {
        _source = std::to_string(static_cast<int>(value));
    }
}

ValueOverflowException::ValueOverflowException(std::string const &value) {
    _message = "Value overflow.";
    _type = "Runtime";
    _source = value.length() > 50 ? value.substr(0, 57) + "..." : value;
}

ValueUnderflowException::ValueUnderflowException(long double value, eOperandType type) {
    _message = "Value underflow.";
    _type = "Runtime";
    if (type == Float || type == Double) {
        _source = std::to_string(value);
    } else {
        _source = std::to_string(static_cast<int>(value));
    }
}

BadStringFormatException::BadStringFormatException(const std::string &value) {
    _message = "Malformated string.";
    _source = value;
    _type = "Runtime";
}

ValueInfException::ValueInfException() {
    _message = "Infinite value.";
    _type = "Runtime";
    _source = "inf";
}

ValueNaNException::ValueNaNException() {
    _message = "Not a number.";
    _type = "Runtime";
    _source = "NaN";
}


DivisionByZeroException::DivisionByZeroException(const double value, eOperandType type) {
    _message = "Division by 0.";
    _type = "Runtime";
    if (type == Float || type == Double) {
        _source = std::to_string(value) + " / 0";
    } else {
        _source = std::to_string(static_cast<long>(value)) + " / 0";
    }
}


TooLongStringException::TooLongStringException(const std::string &s) {
    _message = "String is too long.";
    _type = "Runtime";
    _source = s;
}

BadAllocException::BadAllocException() {
    _message = "Memory allocation failed.";
    _type = "Fatal";
}


