//
// Created by rcepre on 03/12/2020.
//

#pragma once

#include <ostream>
#include "IOperand.hpp"
#include "BaseException.hpp"


// Exceptions with values
class ValueOverflowException : public BaseException {
public:
    ValueOverflowException(long double value, eOperandType type);
    explicit ValueOverflowException(std::string const &value);
};

class BadAllocException : public BaseException {
public:
    BadAllocException();
};

class ValueUnderflowException : public BaseException {
public:
    ValueUnderflowException(long double value, eOperandType type);
};


class BadStringFormatException : public BaseException {
public:
    explicit BadStringFormatException(std::string const &value);
};

class ValueInfException : public BaseException {
public:
    ValueInfException();
};

class ValueNaNException : public BaseException {
public:
    ValueNaNException();
};

class DivisionByZeroException : public BaseException {
public:
    DivisionByZeroException(double value, eOperandType type);
};


class TooLongStringException : public BaseException {
public:
    explicit TooLongStringException(std::string const &s);
};
