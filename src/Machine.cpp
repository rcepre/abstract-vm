//     /   |/ __  \ 42 Lyon School                    
//    / /| |`' / /' / /_ _____  ___   rcepre      
//   / /_| |  / /  / / // / _ \/ _ \  abstract-vm
//   \___  |./ /__/_/\_, /\___/_//_/  08/12/2020
//       |_/\_____/  Machine.cpp

#include "Machine.hpp"
#include "Operand.hpp"

Machine::Machine() : _exit(false) {}

Machine::~Machine() {
    for (const IOperand *o: _stack) {
        delete o;
    }
}

[[maybe_unused]] size_t Machine::getStackSize() const {
    return _stack.size();
}

bool Machine::exited() const {
    return _exit;
}

void Machine::run(const Action &a) {
    if (_exit) { throw Machine::RunAfterExitException(); }

    switch (a.getCommand()) {
        case Action::Add ... Action::Mod:
            _doOp(a.getCommand());
            break;

        case Action::Push:
            _push(a.getType(), a.getValue());
            break;
        case Action::Pop:
            _pop();
            break;

        case Action::Assert:
            _assert(a.getType(), a.getValue());
            break;

        case Action::Dump:
            _dump();
            break;
        case Action::DumpH:
            _dumpH();
            break;
        case Action::Print:
            _print();
            break;

        case Action::Exit:
            _exit = true;
            break;
    }
}

void Machine::_push(eOperandType type, const std::string &value) {
    const IOperand *op = OperandFactory::create(type, value);
    _stack.push_back(op);

}

void Machine::_pop() {

    if (_stack.empty()) {
        throw StackEmptyException();
    }
    delete _stack.back();
    _stack.pop_back();
}

void Machine::_doOp(Action::eCommand op) {
    assert(op >= Action::Add && op <= Action::Mod);
    static const std::function<const IOperand *(const IOperand *, const IOperand *)> _opTab[]{
            [](auto a, auto b) { return *a + *b; },
            [](auto a, auto b) { return *a - *b; },
            [](auto a, auto b) { return *a * *b; },
            [](auto a, auto b) { return *a / *b; },
            [](auto a, auto b) { return *a % *b; },
    };

    if (_stack.empty()) {
        throw StackEmptyException();
    }
    if (_stack.size() < 2) {
        throw LackOfOperandsException();
    }
    size_t last = _stack.size() - 1;
    IOperand const *tmp = _opTab[op](_stack[last - 1], _stack[last]);
    delete _stack[last];
    delete _stack[last - 1];
    _stack.pop_back();
    _stack.pop_back();
    _stack.push_back(tmp);

}

void Machine::_dump() const {
    for (auto it = _stack.rbegin(); it != _stack.rend(); ++it) {
        std::cout << ((*it)->toString()) << std::endl;
    }
}

void Machine::_dumpH() const {
    for (auto it = _stack.rbegin(); it != _stack.rend(); ++it) {
        std::cout << std::setw(7) << Syntax::opToStr(((*it)->getType())) << " [ ";
        std::cout << std::setw(10) << ((*it)->toString()) << " ]" << std::endl;
    }
}

void Machine::_assert(eOperandType type, const std::string &value) const {

    if (_stack.empty()) { throw StackEmptyException(); }

    const IOperand *op = OperandFactory::create(type, value);
    const IOperand *op2 = _stack.back();

    if (op->getType() != op2->getType()) {
        eOperandType t1 = op->getType();
        eOperandType t2 = op2->getType();
        delete op;
        throw Machine::AssertionTypeFailException(t1, t2);
    }

    auto val1 = getValueFromInterface(*op);
    auto val2 = getValueFromInterface(*op2);

    if (val1 != val2) {
        const std::string s1 = op->toString();
        const std::string &s2 = op2->toString();
        delete op;
        throw Machine::AssertionValueFailException(s1, s2);
    }
    delete op;
}

void Machine::_print() {
    if (_stack.empty()) {
        throw StackEmptyException();
    }
    if (_stack.back()->getType() != Int8) {
        throw UnprintableValueException(_stack.back()->getType());
    }
    std::cout << static_cast<char>(getValueFromInterface(*_stack.back()));
}

// EXCEPTIONS

Machine::StackEmptyException::StackEmptyException() {
    _message = "Stack is Empty.";
    _type = "Runtime";
}

Machine::AssertionTypeFailException::AssertionTypeFailException(eOperandType a, eOperandType b) {
    _message = "Assertion failed : types do not match.";
    const std::string &t1 = Syntax::opToStr(a);
    const std::string &t2 = Syntax::opToStr(b);
    _source = t1 + " <> " + t2;
    _type = "Runtime";
}

Machine::AssertionValueFailException::AssertionValueFailException(const std::string &a, const std::string &b) {
    _message = "Assertion failed : values do not match.";
    _source = a + " <> " + b;
    _type = "Runtime";
}

Machine::UnprintableValueException::UnprintableValueException(eOperandType type) {
    _message = "Value must be Int8 type.";
    _source = Syntax::opToStr(type);
    _type = "Runtime";
}

Machine::LackOfOperandsException::LackOfOperandsException() {
    _message = "Not enough operands on stack.";
    _type = "Runtime";
}

Machine::NoExitException::NoExitException() {
    _message = "Exit command is missing.";
    _level = Warning;
}

Machine::RunAfterExitException::RunAfterExitException() {
    _message = "Instructions after the Exit command will be ignored.";
    _level = Warning;
}
