//     /   |/ __  \ 42 Lyon School                    
//    / /| |`' / /' / /_ _____  ___   rcepre      
//   / /_| |  / /  / / // / _ \/ _ \  abstract-vm
//   \___  |./ /__/_/\_, /\___/_//_/  08/12/2020
//       |_/\_____/  Action.hpp

#pragma once

#include <ostream>
#include <regex>
#include "IOperand.hpp"
#include "BaseException.hpp"


class Action {
public:
    enum eCommand {
        Add, Sub, Mul, Div, Mod, Push, Pop, Dump, DumpH, Assert, Print, Exit
    };

private:
    eCommand _command;
    eOperandType _type;
    std::string _value;

public:
    explicit Action(eCommand action);

    Action(eCommand action, eOperandType type, std::string value);

    Action() = default;

    Action(const Action &) = default;

    virtual ~Action() = default;

    Action &operator=(Action const &rhs) = default;

public:

    eCommand getCommand() const;

    eOperandType getType() const;

    const std::string &getValue() const;

    void setType(eOperandType type, int line);

    void setValue(const std::string &value, int line);

    void setCommand(eCommand action);

    bool checkValidity(int line);

private:
    bool _hasParams();

public:
    class UnexpectedException : public BaseException {
    public:
        UnexpectedException(std::string const &s, int line);;
    };

    class InvalidIntegerValueException : public BaseException {
    public:
        InvalidIntegerValueException(std::string const &s, int line);;
    };

    class InvalidFloatValueException : public BaseException {
    public:
        InvalidFloatValueException(std::string const &s, int line);;
    };

    class MissingParameterException : public BaseException {
    public:
        explicit MissingParameterException(int line);
    };

};




