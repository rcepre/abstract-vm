//     /   |/ __  \ 42 Lyon School
//    / /| |`' / /' / /_ _____  ___   rcepre
//   / /_| |  / /  / / // / _ \/ _ \  Exception
//   \___  |./ /__/_/\_, /\___/_//_/  18/12/2020
//       |_/\_____/  Operand.tpp

#pragma once

#include <iomanip>


//CONSTRUCTORS
template<typename T>
Operand<T>::Operand(T value) : _value(value) {
    setRepr();
}

template<typename T>
void Operand<T>::setRepr() {
    std::stringstream ss;
    int p = std::numeric_limits<T>::max_digits10 - 2;

    if (_type == Float || _type == Double) {
        if (roundf(_value) == _value) {
            ss << _value << ".00";
        } else {
            ss << std::setprecision(p) << _value;
        }
    } else {
        ss << +_value;                                     // '+' promotes x to a type printable as a number.
    }
    _repr = ss.str();
}

//GETTERS
template<typename T>
int Operand<T>::getPrecision() const {
    return this->_type;
}

template<typename T>
eOperandType Operand<T>::getType() const {
    return this->_type;
}

template<typename T>
T Operand<T>::getValue() const {
    return _value;
}

// STRING REPRESENTATION

template<typename T>
std::string const &Operand<T>::toString() const {
    return _repr;
}

// INITIALIZE STATICS
template<> inline const eOperandType Operand<int8_t>::_type = Int8;
template<> inline const eOperandType Operand<int16_t>::_type = Int16;
template<> inline const eOperandType Operand<int32_t>::_type = Int32;
template<> inline const eOperandType Operand<float>::_type = Float;
template<> inline const eOperandType Operand<double>::_type = Double;


template<typename T>
[[maybe_unused]] const std::function<long double(double, double)> Operand<T>::_opTab[] = {
        [](double a, double b) { return a + b; },
        [](double a, double b) { return a - b; },
        [](double a, double b) { return a * b; },
        [](double a, double b) {
            if (b == 0) { throw DivisionByZeroException(a, Operand<T>::_type); }
            return a / b;
        },
        [](double a, double b) {
            if (b == 0) { throw DivisionByZeroException(a, Operand<T>::_type); }
            return fmodl(a, b);
        }
};


template<typename T>
void Operand<T>::checkLimits(long double value) {
    if (_type == Float || _type == Double) {
        if (isinf(value)) throw ValueInfException();
        if (isnan(value)) throw ValueNaNException();
        if (value != 0 && fabsl(value) < std::numeric_limits<T>::min()) {
            throw ValueUnderflowException(value, _type);
        }
    }
    if (value > std::numeric_limits<T>::max() || value < std::numeric_limits<T>::lowest()) {
        throw ValueOverflowException(value, _type);
    }
}
// OPERATORS OVERLOADS

//HELPER
static long double getValueFromInterface(IOperand const &i) {
    if (i.getType() == Int8) {
        auto op = dynamic_cast<Operand <int8_t> const &>(i);
        return op.getValue();
    } else if (i.getType() == Int16) {
        auto op = dynamic_cast<Operand <int16_t> const &>(i);
        return op.getValue();
    } else if (i.getType() == Int32) {
        auto op = dynamic_cast<Operand<int> const &>(i);
        return op.getValue();
    } else if (i.getType() == Float) {
        auto op = dynamic_cast<Operand<float> const &>(i);
        return op.getValue();
    } else {
        auto op = dynamic_cast<Operand<double> const &>(i);
        return op.getValue();
    }
}


template<typename T>
IOperand const *Operand<T>::doOp(const IOperand &rhs, eOpType op) const {

    double rhsValue = static_cast<double>(getValueFromInterface(rhs));
    int pre = rhs.getPrecision() > this->_type ? rhs.getPrecision() : this->_type;
    eOperandType targetType = (eOperandType) pre;


    if (targetType == Float || targetType == Double) {
        long double result = _opTab[op](this->_value, rhsValue);
        return OperandFactory::create(targetType, std::to_string(result));
    } else {
        long result = static_cast<long>(_opTab[op](this->_value, rhsValue));
        return OperandFactory::create(targetType, std::to_string(result));
    }
}


template<typename T>
IOperand const *Operand<T>::operator+(IOperand const &rhs) const {
    return doOp(rhs, Add);
}

template<typename T>
IOperand const *Operand<T>::operator-(IOperand const &rhs) const {
    return doOp(rhs, Sub);
}

template<typename T>
IOperand const *Operand<T>::operator*(IOperand const &rhs) const {
    return doOp(rhs, Mul);
}

template<typename T>
IOperand const *Operand<T>::operator/(IOperand const &rhs) const {
    return doOp(rhs, Div);
}

template<typename T>
IOperand const *Operand<T>::operator%(const IOperand &rhs) const {
    return doOp(rhs, Mod);
}



