#include <iostream>
#include <fstream>
#include "IOperand.hpp"
#include "Action.hpp"
#include "Machine.hpp"
#include "Parser.hpp"

std::fstream readFromFile(std::string const &fileName) {

    std::fstream file;

    if (fileName.substr(fileName.find_last_of('.') + 1) != "avm") {
        throw BaseException("Bad file extension.");
    }

    file.open(fileName);
    if (file.fail()) {
        throw BaseException("Can't open the file.");
    }

    return file;
}

int main(int ac, char **av) {

    try {
        std::string buf;
        if (ac == 2) {
            std::fstream file = readFromFile(av[1]);
            while (std::getline(file, buf)) {
                Parser::readLine(buf);
            }

        } else if (ac == 1) {
            while (buf != ";;") {
                std::getline(std::cin, buf);
                Parser::readLine(buf);
            }
        } else {
            throw BaseException("Too many arguments.");
        }
    } catch (BaseException &e) {
        std::cout << e.pretty() << std::endl;
        return 0;
    }


    if (Parser::hasErrors()) {
        for (BaseException const &e: Parser::getErrors()) {
            std::cout << e.pretty() << std::endl;
        }
    } else {
        try {
            Machine m;
            for (Action const &a: Parser::getActions()) {
                m.run(a);
            }
            if (!m.exited()) {
                throw Machine::NoExitException();
            }
        } catch (BaseException &e) {
            std::cout << e.pretty() << std::endl;
        }
    }
    return 0;
}
