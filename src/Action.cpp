//     /   |/ __  \ 42 Lyon School                    
//    / /| |`' / /' / /_ _____  ___   rcepre      
//   / /_| |  / /  / / // / _ \/ _ \  abstract-vm
//   \___  |./ /__/_/\_, /\___/_//_/  08/12/2020
//       |_/\_____/  Action.cpp

#include "Action.hpp"


Action::Action(eCommand action, eOperandType type, std::string value) :
        _command(action), _type(type), _value(std::move(value)) {
}

Action::Action(eCommand action) : _command(action) {
}

Action::eCommand Action::getCommand() const {
    return _command;
}

eOperandType Action::getType() const {
    return _type;
}

const std::string &Action::getValue() const {
    return _value;
}

void Action::setCommand(Action::eCommand action) {
    _command = action;
}

void Action::setType(eOperandType type, int line) {
    if (!_hasParams()) {
        throw Action::UnexpectedException(_value, line);
    }
    _type = type;
}

void Action::setValue(const std::string &value, int line) {
    if (!_hasParams()) {
        throw Action::UnexpectedException(value, line);
    }
    _value = value;
}

bool Action::checkValidity(int line) {
    if (!_hasParams()) {
        return true;
    }
    if (_value.empty()) {
        throw Action::MissingParameterException(line);
    }

    if (_type == Float || _type == Double) {
        if (!std::regex_match(_value, std::regex("[-]?[0-9]+\\.[0-9]+"))) {
            throw Action::InvalidFloatValueException(_value, line);
        }
    } else {
        if (!std::regex_match(_value, std::regex("[-]?[0-9]+"))) {
            throw Action::InvalidIntegerValueException(_value, line);
        }
    }
    return true;
}

bool Action::_hasParams() {
    return (_command == Action::Push || _command == Action::Assert);
}

// EXCEPTIONS

Action::UnexpectedException::UnexpectedException(const std::string &s, int line) {
    _source = s;
    std::string l = "line " + std::to_string(line) + ": ";
    _message = l + "Unexpected token.";
    _type = "Parsing";
}

Action::InvalidIntegerValueException::InvalidIntegerValueException(const std::string &s, int line) {
    _source = s;
    std::string l = "line " + std::to_string(line) + ": ";
    _message = l + "Malformed integer value.";
    _type = "Parsing";
}

Action::InvalidFloatValueException::InvalidFloatValueException(const std::string &s, int line) {
    _source = s;
    std::string l = "line " + std::to_string(line) + ": ";
    _message = l + "Malformed float value.";
    _type = "Parsing";
}

Action::MissingParameterException::MissingParameterException(int line) {
    std::string l = "line " + std::to_string(line) + ": ";
    _message = l +  "Missing parameters.";
    _type = "Parsing";
}
