
#include "BaseException.hpp"


BaseException::BaseException(std::string msg, std::string source, BaseException::Lvl lvl)
        : _message(std::move(msg)),
          _source(std::move(source)),
          _level(lvl) {

}

BaseException::BaseException() : _level(Error) {

}

std::string BaseException::pretty() const {
    std::string s;
    std::string source;
    std::string type;

    if (_source.length() > 50) {
        source = _source.substr(0, 47) + "...";
    } else {
        source = _source;
    }
    if (!_type.empty()) {
        type = _type + ": ";
    }
    s = lvlStr() + type + _message;
    if (!_source.empty()) {
        s += " ( " + source + " )";
    }
    return s;
}

const char *BaseException::what() const noexcept {
    return _message.c_str();
}

std::string BaseException::lvlStr() const {
    if (_level == Info) {
        return "\033[1;34mInfo: \033[0m";
    } else if (_level == Error) {
        return "\033[1;31mError: \033[0m";
    } else if (_level == Warning) {
        return "\033[1;33mWarning: \033[0m";
    }
    return "";
}