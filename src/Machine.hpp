//     /   |/ __  \ 42 Lyon School                    
//    / /| |`' / /' / /_ _____  ___   rcepre      
//   / /_| |  / /  / / // / _ \/ _ \  abstract-vm
//   \___  |./ /__/_/\_, /\___/_//_/  08/12/2020
//       |_/\_____/  Machine.hpp

#pragma once

#include <ostream>
#include "Action.hpp"
#include <vector>
#include <functional>
#include "IOperand.hpp"
#include "OperandFactory.hpp"
#include "OperandsExceptions.hpp"
#include "BaseException.hpp"
#include "Syntax.hpp"


class Machine {
private:
    std::vector<const IOperand *> _stack;
    bool _exit;

public:
    Machine();

    virtual ~Machine();

    Machine(Machine const &o) = default;

    Machine &operator=(Machine const &rhs) = default;

public:
    bool exited() const;

    void run(const Action &a);

    [[maybe_unused]] size_t getStackSize() const;

private:

    void _push(eOperandType type, std::string const &value);

    void _print();

    void _assert(eOperandType type, std::string const &value) const;

    void _dump() const;

    void _dumpH() const;

    void _pop();

    void _doOp(Action::eCommand op);

public:
    class StackEmptyException : public BaseException {
    public:
        StackEmptyException();
    };

    class RunAfterExitException: public BaseException {
    public:
        RunAfterExitException();
    };

    class AssertionTypeFailException : public BaseException {
    public:
        AssertionTypeFailException(eOperandType a, eOperandType b);
    };

    class AssertionValueFailException : public BaseException {
    public:
        AssertionValueFailException(std::string const &a, std::string const &b);
    };

    class UnprintableValueException : public BaseException {
    public:
        explicit UnprintableValueException(eOperandType type);
    };

    class LackOfOperandsException : public BaseException {
    public:
        LackOfOperandsException();
    };

    class NoExitException : public BaseException {
    public:
        NoExitException();
    };
};


