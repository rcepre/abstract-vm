//     /   |/ __  \ 42 Lyon School                    
//    / /| |`' / /' / /_ _____  ___   rcepre      
//   / /_| |  / /  / / // / _ \/ _ \  abstract-vm
//   \___  |./ /__/_/\_, /\___/_//_/  15/12/2020
//       |_/\_____/  Syntax.hpp

#pragma once


#include <string>
#include <array>
#include <unordered_map>
#include "IOperand.hpp"
#include "Action.hpp"

namespace Syntax {

    std::string const &whitespaces();

    std::string const &separators();

    eOperandType strToOp(std::string const &str);

    std::string const &opToStr(eOperandType op);

    Action::eCommand strToCommand(std::string const &str);
}

//ABSTRACT_VM_SYNTAX_HPP


