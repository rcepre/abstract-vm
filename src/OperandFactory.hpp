//
// Created by rcepre on 27/11/2020.
//

#pragma once

#include <ostream>
#include <array>
#include "IOperand.hpp"


class OperandFactory {

    typedef IOperand const *(OperandFactory::*fptr)(std::string const &) const;

private:
    std::array<fptr, NB_OPT_TYPES> fPointers{};

public:

    OperandFactory();

    OperandFactory(OperandFactory const &o) = delete;

    virtual ~OperandFactory() = default;

    OperandFactory &operator=(OperandFactory const &rhs) = delete;

public:

    static OperandFactory const &get();

    static IOperand const *create(eOperandType type, std::string const &value);


private:
    IOperand const *createInt8(std::string const &value) const;

    IOperand const *createInt16(std::string const &value) const;

    IOperand const *createInt32(std::string const &value) const;

    IOperand const *createFloat(std::string const &value) const;

    IOperand const *createDouble(std::string const &value) const;

    IOperand const *createOperand(eOperandType type, std::string const &value) const;
};





