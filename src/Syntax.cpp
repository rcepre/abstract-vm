//
// Created by Renaud Cepre on 1/6/21.
//

#include "Syntax.hpp"

Action::eCommand Syntax::strToCommand(const std::string &str) {
    const std::unordered_map<std::string, Action::eCommand> data = {
            {"push",   Action::Push},
            {"pop",    Action::Pop},
            {"assert", Action::Assert},
            {"add",    Action::Add},
            {"sub",    Action::Sub},
            {"div",    Action::Div},
            {"mul",    Action::Mul},
            {"mod",    Action::Mod},
            {"exit",   Action::Exit},
            {"dump",   Action::Dump},
            {"dumph",   Action::DumpH},
            {"print",  Action::Print}
    };
    return data.at(str);
}

std::string const &Syntax::opToStr(eOperandType op) {
    static const std::unordered_map<eOperandType, std::string> data = {
            {Int8,   "int8"},
            {Int16,  "int16"},
            {Int32,  "int32"},
            {Float,  "float"},
            {Double, "double"},
    };
    return data.at(op);
}

eOperandType Syntax::strToOp(const std::string &str) {
    static const std::unordered_map<std::string, eOperandType> data = {
            {"int8",   Int8},
            {"int16",  Int16},
            {"int32",  Int32},
            {"float",  Float},
            {"double", Double},
    };
    return data.at(str);
}

std::string const &Syntax::whitespaces() {
    static const std::string s = " \n\r\t\f\v";
    return s;
}

std::string const &Syntax::separators() {
    static const std::string s = whitespaces() + "()";
    return s;
}
