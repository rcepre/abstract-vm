//     /   |/ __  \ 42 Lyon School                    
//    / /| |`' / /' / /_ _____  ___   rcepre      
//   / /_| |  / /  / / // / _ \/ _ \  Exception
//   \___  |./ /__/_/\_, /\___/_//_/  18/12/2020
//       |_/\_____/  BaseException.hpp

#pragma once

#include <ostream>
#include <iostream>


class BaseException : public std::exception {
public:
    enum Lvl {
        Error, Warning, Info
    };

protected:
    std::string _message;
    std::string _type;
    std::string _source;
    Lvl _level;

public:
    BaseException();

    BaseException(BaseException const &) = default;

    BaseException &operator=(BaseException const &) = default;

    ~BaseException() override = default;

public:

    explicit BaseException(std::string msg, std::string source = "", Lvl lvl = Error);

    std::string pretty() const;

    const char *what() const noexcept override;

private:
    std::string lvlStr() const;
};