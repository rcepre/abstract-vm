//     /   |/ __  \ 42 Lyon School                    
//    / /| |`' / /' / /_ _____  ___   rcepre      
//   / /_| |  / /  / / // / _ \/ _ \  abstract-vm
//   \___  |./ /__/_/\_, /\___/_//_/  11/12/2020
//       |_/\_____/  Parser.cpp

#include "Parser.hpp"
#include "Syntax.hpp"

// HELPERS
static void cleanLine(std::string &s) {

    if (!s.empty()) {

        //trim left side whitespaces
        size_t start = s.find_first_not_of(Syntax::whitespaces());
        s = (start == std::string::npos) ? "" : s.substr(start);

        //trim comments
        size_t end = s.find_first_of(';');
        if (end != std::string::npos) {
            s = s.substr(0, end);
        }
        //trim right side whitespaces
        end = s.find_last_not_of(Syntax::whitespaces());
        s = (end == std::string::npos) ? "" : s.substr(0, end + 1);
    }
}

// PARSERS
Action::eCommand Parser::_parseCommandKeyWord(std::string &s) {

    std::string expr;

//    Parse first expression of the line. Must be a Command.
    size_t start = s.find_first_not_of(Syntax::whitespaces());
    expr = (start == std::string::npos) ? "" : s.substr(start);

    size_t end = s.find_first_of(Syntax::separators());
    expr = (end == std::string::npos) ? expr : s.substr(0, end);

    s = (end != std::string::npos) ? s = s.substr(end) : "";

    try {
        return Syntax::strToCommand(expr);
    } catch (std::out_of_range &e) {
        throw Parser::InvalidCommandException(expr);
    }
}

eOperandType Parser::_parseOpTypeKeyWord(std::string &s) {


    size_t start = s.find_first_not_of(Syntax::whitespaces());
    s = (start == std::string::npos) ? "" : s.substr(start);

    size_t end = s.find_first_of(Syntax::separators());
    std::string expr = s.substr(0, end);

    s = (end != std::string::npos) ? s = s.substr(end) : "";

    if (expr.empty()) {
        throw Parser::InvalidOpTypeException(expr);
    }

    try {
        return Syntax::strToOp(expr);
    } catch (std::out_of_range &e) {
        throw Parser::InvalidOpTypeException(expr);
    }
}

std::string Parser::_parseValue(std::string &s) {

    size_t start = s.find_first_of('(');
    size_t end = s.find_first_of(')');

    if (start == std::string::npos || end == std::string::npos) {
        throw Parser::InvalidValueException(s);
    }
    std::string tmp = s.substr(start + 1, end - 1);
    s = s.substr(end + 1);
    return tmp;
}

// READ
void Parser::readLine(std::string const &str) {
    Parser::get()._readLine(str);
}

void Parser::_readLine(std::string const &str) {
    _lineCount++;
    std::string tmp = str;
    Action a;
    cleanLine(tmp);
    bool isValid = true;

    if (tmp.empty()) {
        return; // Do nothing if the string is just a newline or comment.
    }

    try {
        a.setCommand(_parseCommandKeyWord(tmp));
    } catch (Parser::InvalidCommandException &e) {
        Parser::addError(e);
        isValid = false;
    }

    if (isValid && !tmp.empty()) {
        try {
            a.setType(_parseOpTypeKeyWord(tmp), _lineCount);
        } catch (Parser::InvalidOpTypeException &e) {
            Parser::addError(e);
            isValid = false;
        } catch (Action::UnexpectedException &e) {
            Parser::addError(e);
            isValid = false;
        }
    }

    if (isValid && !tmp.empty()) {
        try {
            a.setValue(_parseValue(tmp), _lineCount);
        } catch (Parser::InvalidValueException &e) {
            Parser::addError(e);
            isValid = false;
        }
    }

    if (isValid && tmp.find_first_not_of(Syntax::whitespaces()) != std::string::npos) {
        std::string errorStr = tmp.substr(tmp.find_first_not_of(Syntax::whitespaces()));
        Parser::addError(Parser::UnexpectedExpressionException(errorStr));
        isValid = false;
    }

    if (isValid) {
        try {
            if (a.checkValidity(_lineCount)) {
                _actions.push_back(a);
            }
        } catch (BaseException &e) {
            Parser::addError(e);
        }
    }
}

Parser &Parser::Parser::get() {
    static Parser instance;
    return instance;
}

const std::vector <Action> &Parser::_getActions() const {
    return _actions;
}

const std::vector <Action> &Parser::getActions() {
    return get()._getActions();
}

void Parser::addError(const BaseException &e) {
    return get()._addError(e);
}

bool Parser::hasErrors() {
    return get()._hasErrors();
}

std::vector <BaseException> Parser::getErrors() {
    return get()._getErrors();
}

void Parser::_addError(const BaseException &e) {
    if (Parser::_errors.size() > 65536) {
        throw BaseException("Too much errors. (Exceed 65536)");
    }
    _errors.push_back(e);
}

bool Parser::_hasErrors() const {
    return !_errors.empty();
}

std::vector <BaseException> Parser::_getErrors() const {
    return _errors;
}


Parser::InvalidCommandException::InvalidCommandException(const std::string &s) {
    _source = s;
    std::string line = "line " + std::to_string(_lineCount) + ": ";
    _message = line + "Invalid command.";
    _type = "Parsing";
}

Parser::InvalidOpTypeException::InvalidOpTypeException(const std::string &s) {
    _source = s;
    std::string line = "line " + std::to_string(_lineCount) + ": ";
    _message = line + "Invalid operand type.";
    _type = "Parsing";
}

Parser::InvalidValueException::InvalidValueException(const std::string &s) {
    _source = s;
    std::string line = "line " + std::to_string(_lineCount) + ": ";
    _message = line + "Invalid value.";
    _type = "Parsing";
}

Parser::UnexpectedExpressionException::UnexpectedExpressionException(const std::string &s) {
    _source = s;
    std::string line = "line " + std::to_string(_lineCount) + ": ";
    _message = line + "Unexpected expression.";
    _type = "Parsing";
}
