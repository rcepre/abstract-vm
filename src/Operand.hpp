//     /   |/ __  \ 42 Lyon School
//    / /| |`' / /' / /_ _____  ___   rcepre
//   / /_| |  / /  / / // / _ \/ _ \  Exception
//   \___  |./ /__/_/\_, /\___/_//_/  18/12/2020
//       |_/\_____/  Operand.hpp

#pragma once

#include "IOperand.hpp"
#include <limits>
#include <cassert>
#include <cmath>
#include <memory>
#include "OperandsExceptions.hpp"
#include <functional>
#include <sstream>
#include "OperandFactory.hpp"


//DECLARATION
template<typename T>
class Operand :
        public IOperand {
public:
    enum eOpType {
        Add, Sub, Mul, Div, Mod
    };

private:

    const T _value;
    std::string _repr;
    static const eOperandType _type;
    static const std::function<long double(double, double)> _opTab[];

public:

    Operand() = delete;

    explicit Operand(T value);

    Operand(Operand const &cp) = default;

    Operand &operator=(Operand const &rhs) = default;


public:

    IOperand const *operator+(IOperand const &rhs) const override;

    IOperand const *operator-(IOperand const &rhs) const override;

    IOperand const *operator*(IOperand const &rhs) const override;

    IOperand const *operator/(IOperand const &rhs) const override;

    IOperand const *operator%(IOperand const &rhs) const override;

    [[nodiscard]] std::string const &toString() const override;

    [[nodiscard]] int getPrecision() const override;

    [[nodiscard]] eOperandType getType() const override;

    [[nodiscard]] T getValue() const;

    IOperand const *doOp(IOperand const &rhs, eOpType op) const;

private:
    void setRepr();


public:
    static void checkLimits(long double value);
};

//DEFINITIONS
#include "Operand.tpp"

